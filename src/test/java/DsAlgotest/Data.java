package DsAlgotest;

import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
//import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import DsAlgopages.DSAlgoBrowser;
import DsAlgopages.DSlogin;
//import DsAlgopages.DSlogin;
import DsAlgopages.DataStructure;

public class Data extends DSAlgoBrowser {
	public DSlogin login = new DSlogin();

	@BeforeTest
	public void Setup() {
		Browser_open();
		lwaittime();

	}

	@Test(dataProvider = "UserRegistedLogin")
	public void DataStructure(String user, String password1) {

		login.GetStarted();
		login.signin();
		login.Userpass(user, password1);
		login.login1();

		DataStructure DataStruct = new DataStructure();
		DataStruct.data();
		mwaittime();
		DataStruct.DataTimeComplexity();
		mwaittime();
		DataStruct.DataTry();
		mwaittime();
		DataStruct.Dataprint();
		lwaittime();
		DataStruct.DataRun();
		DataStruct.Dataoutputverification();
		Browser2_Back();
		//DataStruct.linked();
		login.signout();
}
	@AfterTest
	public void close()
	{
		Browser_close();
	}

}
