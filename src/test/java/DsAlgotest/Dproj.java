package DsAlgotest;

import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;


import DsAlgopages.DSAlgoBrowser;
import DsAlgopages.DSGraph;
import DsAlgopages.DSLinkedlist;
import DsAlgopages.DSQueue;
import DsAlgopages.DSlogin;
import DsAlgopages.DataStructure;

public class Dproj extends DSAlgoBrowser {
	DSlogin login = new DSlogin();
	DataStructure DataStruct = new DataStructure();
	DSLinkedlist Li = new DSLinkedlist();
	DSQueue Qu = new DSQueue();
	DSGraph Gr = new DSGraph();
	/*
	 * @BeforeTest
	 *
	 * @Parameters("Browser") public void browser(String browserName) {
	 * Browser_open(browserName); }
	 */

	@Test(dataProvider = "LoginData")
	public void DSlogin(String user, String password1) {

		login.GetStarted();
		login.signin();
		login.Userpass(user, password1);
		login.login1();
		login.userverification();

		DataStruct.data();
		mwaittime();
		DataStruct.DataTimeComplexity();
		mwaittime();
		DataStruct.DataTry();
		mwaittime();
		DataStruct.Dataprint();
		lwaittime();
		DataStruct.DataRun();
		DataStruct.Dataoutputverification();
		Browser2_Back();

		DataStruct.linked();
		Li.Linkedlist();
		Li.LinkedIntroduction();
		Li.LinkedSteps();
		Browser2_Back();
		mwaittime();
		Li.LinkedCreation();
		Li.LinkedSteps();
		Browser2_Back();
		mwaittime();
		Li.LinkedTypes();
		Li.LinkedSteps();
		Browser2_Back();
		mwaittime();
		Li.LinkedImplement();
		Li.LinkedSteps();
		Browser2_Back();
		mwaittime();
		Li.LinkedTraversal();
		Li.LinkedSteps();
		Browser2_Back();
		mwaittime();
		Li.LinkedInsertion();
		Li.LinkedSteps();
		Browser2_Back();
		mwaittime();
		Li.LinkedDeletion();
		Li.LinkedSteps();
		Browser2_Back();
		mwaittime();

		Li.Queue();
		Qu.Queue();
		mwaittime();
		Qu.QueueImplemantation();
		mwaittime();
		Qu.QueueSteps();
		Browser2_Back();
		Qu.QueueCollections();
		mwaittime();
		Qu.QueueSteps();
		mwaittime();
		Browser2_Back();
		Qu.QueueArray();
		Qu.QueueSteps();
		Browser2_Back();
		Qu.QueueOperations();
		Qu.QueueSteps();
		Browser2_Back();

		Qu.Graph();
		Gr.Dgraph();
		mwaittime();
		Gr.GraphElement();
		Gr.GraphSteps();
		Browser2_Back();
		Gr.GraphRepresentations();
		Gr.GraphSteps();
		Browser2_Back();
		login.signout();
		}
		
		  @AfterTest 
		  public void Browserclose() 
		  { Browser_close(); 
		  }
		 
}
