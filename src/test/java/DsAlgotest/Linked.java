package DsAlgotest;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import DsAlgopages.DSAlgoBrowser;
import DsAlgopages.DSLinkedlist;
import DsAlgopages.DSlogin;

public class Linked extends DSAlgoBrowser {

		 @BeforeTest
			public void Setup()
			 {
			   Browser_open();
			   lwaittime();
			 }

		 @Test(dataProvider = "UserRegistedLogin")
		public void LinkedDS(String user, String password1)
		 {
		  DSlogin login=new DSlogin();
		  DSLinkedlist Li=new DSLinkedlist();
		  login.GetStarted();
		  mwaittime();
		  login.signin();
		  mwaittime();
		  login.Userpass(user, password1);
		  login.login1();
		  mwaittime();
		  Li.Linkedlist();
		  Li.Linkedlistclick();
		  Li.LinkedIntroduction();
		  Li.LinkedSteps();
		  Browser2_Back();
		  mwaittime();
		  Li.LinkedCreation();
		  Li.LinkedSteps();
		  Browser2_Back();
		  mwaittime();
		  Li.LinkedTypes();
		  Li.LinkedSteps();
		  Browser2_Back();
		  mwaittime();
		  Li.LinkedImplement();
		  Li.LinkedSteps();
		  Browser2_Back();
		  mwaittime();
		  Li.LinkedTraversal();
		  Li.LinkedSteps();
		  Browser2_Back();
		  mwaittime();
		  Li.LinkedInsertion();
		  Li.LinkedSteps();
		  Browser2_Back();
		  mwaittime();
		  Li.LinkedDeletion();
		  Li.LinkedSteps();
		  Browser2_Back();
		  mwaittime();
		  Li.Queue();
	 }

}
