package DsAlgotest;


import org.testng.annotations.AfterTest;
//import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import DsAlgopages.DRegister;
import DsAlgopages.DSAlgoBrowser;
//import DsAlgopages.DSlogin;


public class Registration extends DSAlgoBrowser{

   @BeforeTest
   public void Setup()
   {
	 Browser_open();

   }

@Test(dataProvider= "UserRegister")
public void DSRegister(String user,String password1,String password2)
{
	DRegister Register=new DRegister();
	Register.GetStarted();
	Register.Registeruser();
	Register.UsernamePassword(user,password1,password2);
	Register.registerUser();
	Register.verifyingText();
}

/*
 * @Test(dataProvider = "UserRegistedLogin") public void DSlogin(String user,
 * String password1) { DSlogin login = new DSlogin(); login.GetStarted();
 * login.signin(); login.Userpass(user, password1); login.login1();
 * login.userverification(); // login.signout(); }
 */



 @AfterTest
   public void close()
   {
	Browser_close();
   }



}
