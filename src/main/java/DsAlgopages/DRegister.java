package DsAlgopages;


import java.time.Duration;

import org.openqa.selenium.By;



//import org.testng.annotations.Test;

public class DRegister extends DSAlgoBrowser {

	By GetStarted=By.xpath("//*[contains(text( ),'Get Started')]");
	By Registeruser=By.xpath("//*[contains(text( ),'Register')]");
	By Username =By.name("username");
	By Password=By.name("password1");
	By CPassword=By.name("password2");
	By registerUser=By.xpath("//input[@value ='Register']");
	By verifyingText=By.xpath("//div[contains (text( ),'New Account Created')]");


	public void GetStarted()
	{
		driver1.findElement(GetStarted).click();


	}

	public  void Registeruser()
	{
		driver1.findElement(Registeruser).click();
		driver1.manage().timeouts().pageLoadTimeout(Duration.ofSeconds(60));
	}

	public void UsernamePassword(String user,String password1,String password2)
	{
		driver1.findElement(Username).sendKeys(user);
		driver1.findElement(Password).sendKeys(password1);
		driver1.findElement(CPassword).sendKeys(password2);
	}


	public void CPassword()
	{
		
	}

	public void registerUser()
	{
		driver1.findElement(registerUser).click();
	}

	public void verifyingText()
	{
	 String stext;
	if (driver1.findElements(verifyingText).size()!=0)
	{
	  stext=driver1.findElement(verifyingText).getText();
	  System.out.println(stext);
	}else
	{  stext="User is already registered";
		System.out.println(stext);
	}
	driver1.manage().timeouts().pageLoadTimeout(Duration.ofSeconds(100));
	}
	
}

