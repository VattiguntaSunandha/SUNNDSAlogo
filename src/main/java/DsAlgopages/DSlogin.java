package DsAlgopages;

import static org.testng.Assert.assertEquals;

import java.time.Duration;

import org.openqa.selenium.By;

public class DSlogin extends DSAlgoBrowser {

	public String user;

	By GetStarted = By.xpath("//*[contains(text( ),'Get Started')]");
	By signin = By.xpath("//*[contains(text( ),'Sign in')]");
	By Username1 = By.name("username");
	By Password1 = By.name("password");
	By login1 = By.xpath("//input[@type= 'submit' and @value='Login']");
	By Userdetails = By.xpath("//a[contains (text(),'@gmail.com')]");
	By signout = By.xpath("//a[@href='/logout' and text()='Sign out']");
	By Home = By.xpath("//a[@href='/home']");

	public void GetStarted() {
		driver1.findElement(GetStarted).click();
	}

	public void signin() {
		driver1.findElement(signin).click();
		driver1.manage().timeouts().pageLoadTimeout(Duration.ofSeconds(60));
	}

	public void Userpass(String user, String password1) {
		this.user = user;
		//System.out.println(user + " , " + password1);
		driver1.findElement(Username1).sendKeys(user);
		driver1.findElement(Password1).sendKeys(password1);
	}

	public void login1() {
		driver1.findElement(login1).click();
		driver1.manage().timeouts().pageLoadTimeout(Duration.ofSeconds(30));
	}

	public void signout() {
		driver1.findElement(signout).click();
		driver1.manage().timeouts().pageLoadTimeout(Duration.ofSeconds(90));
	}

	public void userverification() {
		if (driver1.findElements(Userdetails).size() != 0) {
			String Userd = driver1.findElement(Userdetails).getText();
			//System.out.println(Userd);
			String userd = Userd.toLowerCase();
			assertEquals(userd, user);
			//System.out.println(user);
			System.out.println(user + " Loged Successfully");
		} else {
			System.out.println("User is not Registered or User/password incorrect");
		}
		driver1.manage().timeouts().pageLoadTimeout(Duration.ofSeconds(100));
	}

	/*
	 * public void Home() { driver1.findElement(Home).click(); }
	 */

}
