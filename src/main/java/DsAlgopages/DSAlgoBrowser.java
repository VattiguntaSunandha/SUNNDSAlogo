package DsAlgopages;


import java.io.IOException;
import java.time.Duration;


import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.DataProvider;

import DsAlgoUtility.Xutility;

public class DSAlgoBrowser {

	public static WebDriver driver1;
	static String baseurl = "https://dsportalapp.herokuapp.com/";
	// static String Home="https://dsportalapp.herokuapp.com/home";

	public static void Browser1_open(String browserName) {
		if (browserName.equalsIgnoreCase("Chrome")) {
			System.setProperty("webdriver.chrome.driver", "C:/Sunandha/drivers/chromedriver.exe");
			driver1 = new ChromeDriver();
		} else if (browserName.equalsIgnoreCase("Firefox")) {
			System.setProperty("webdriver.gecko.driver",
					"C:/Sunandha/Eclipse/DsAlgo/src/test/resources/Drivers/geckodriver.exe");
			driver1 = new FirefoxDriver();

		} else if (browserName.equalsIgnoreCase("Edge")) {
			System.setProperty("webdriver.edge.driver",
					"C:/Sunandha/Eclipse/DsAlgo/src/test/resources/Drivers/msedgedriver.exe");
			driver1 = new EdgeDriver();

		}
		driver1.get(baseurl);
		driver1.manage().window().maximize();
	}

	public static void Browser_open() {
		System.setProperty("webdriver.chrome.driver", "C:/Sunandha/drivers/chromedriver.exe");
		driver1 = new ChromeDriver();
		driver1.get(baseurl);
		driver1.manage().window().maximize();
	}

	public static void lwaittime() {
		driver1.manage().timeouts().pageLoadTimeout(Duration.ofSeconds(60));
	}

	public static void mwaittime() {
		driver1.manage().timeouts().pageLoadTimeout(Duration.ofSeconds(900));
	}

	public static void Home() {
		driver1.navigate().to("https://dsportalapp.herokuapp.com/home");
	}

	public static void Browser2_Back() {
		driver1.navigate().back();
		driver1.navigate().back();

	}

	public static void Browser_Refresh() {
		driver1.navigate().refresh();
	}

	public static void Browser_forward() {
		driver1.navigate().forward();
	}

	public static void Browser_back() {
		driver1.navigate().back();
	}

	public static void Browser_close() {
		// ScreenshotUtility.captureScreenshot(driver1,"TestCase");
		driver1.close();

	}

	@DataProvider(name = "LoginData")
	public String[][] getData() throws IOException {
		System.out.println("Reading Excel Data");
		String path = "C:/Sunandha/Eclipse/DsAlgo/src/test/resources/TestData/LoginData.xlsx";
		Xutility xlutil = new Xutility(path);
		int totalrows = xlutil.getRowCount("Login");
		int totalcols = xlutil.getCellCount("Login", 1);
		// System.out.println(totalrows + "," + totalcols);
		String[][] loginData = new String[totalrows][totalcols];
		for (int i = 1; i <= totalrows; ++i) {
			for (int j = 0; j < totalcols; ++j) {
				// System.out.println("Using login data");
				// System.out.println(i + "," + j);
				loginData[i - 1][j] = xlutil.getCellData("Login", i, j);

			}
		}
		return loginData;
	}

	@DataProvider(name = "UserRegister")
	public String[][] getUserData() throws IOException {
		System.out.println("Registering User from Excel");
		String path = "C:/Sunandha/Eclipse/DsAlgo/src/test/resources/TestData/Userlogin.xlsx";
		Xutility xlutil = new Xutility(path);
		int totalrows = xlutil.getRowCount("Register");
		int totalcols = xlutil.getCellCount("Register", 1);
		// System.out.println(totalrows + "," + totalcols);
		String[][] loginData = new String[totalrows][totalcols];
		for (int i = 1; i <= totalrows; ++i) {
			for (int j = 0; j < totalcols; ++j) {
				// System.out.println("Using login data");
				// System.out.println(i + "," + j);
				loginData[i - 1][j] = xlutil.getCellData("Register", i, j);

			}
		}
		return loginData;
	}

	@DataProvider(name = "UserRegistedLogin")
	public String[][] getUserlData() throws IOException {
		System.out.println("Registering User Login from Excel");
		String path = "C:/Sunandha/Eclipse/DsAlgo/src/test/resources/TestData/Userlogin.xlsx";
		Xutility xlutil = new Xutility(path);
		int totalrows = xlutil.getRowCount("login");
		int totalcols = xlutil.getCellCount("login", 1);
		// System.out.println(totalrows + "," + totalcols);
		String[][] loginData = new String[totalrows][totalcols];
		for (int i = 1; i <= totalrows; ++i) {
			for (int j = 0; j < totalcols; ++j) {
				// System.out.println("Using login data");
				// System.out.println(i + "," + j);
				loginData[i - 1][j] = xlutil.getCellData("login", i, j);

			}
		}
		return loginData;
	}
}
