package DsAlgopages;

import java.time.Duration;

import org.openqa.selenium.By;

public class DataStructure extends DSlogin {

	public String str = "print(900)";

	By data = By.xpath("//a[@href='data-structures-introduction']");
	By DataTimeComplexity = By.xpath("//a[contains (text( ),'Time Complexity') and @href='time-complexity']");
	By DataTry = By.xpath("//a[@href='/tryEditor' and contains (text( ),'Try here>>>')]");
	By Dataprint = By.xpath("//textarea[@autocorrect='off']");
	By DataRun = By.xpath("//button[@type='button' and text()='Run']");
	By Dataoutputverification = By.xpath("//pre[@id='output']");
	By Dropdown=By.xpath("//a[@href='#'and text()='Data Structures']");
	By Linked=By.xpath("//a[@href='/linked-list'and text()='Linked List']");


	public void data() {
		driver1.findElement(data).click();
		driver1.manage().timeouts().pageLoadTimeout(Duration.ofSeconds(100));
		System.out.println("===========================");
		System.out.println("Data Structures");
		System.out.println("===========================");
	}

	public void DataTimeComplexity() {
		driver1.findElement(DataTimeComplexity).click();
		driver1.manage().timeouts().pageLoadTimeout(Duration.ofSeconds(150));
		System.out.println("1.Time Complexity");
	}

	public void DataTry() {
		driver1.findElement(DataTry).click();
		driver1.manage().timeouts().pageLoadTimeout(Duration.ofSeconds(150));

	}

	public void Dataprint() {
		driver1.findElement(Dataprint).sendKeys(str);
	}

	public void DataRun() {
		driver1.findElement(DataRun).click();
		driver1.manage().timeouts().pageLoadTimeout(Duration.ofSeconds(150));

	}

	public void Dataoutputverification() {
		if (driver1.findElements(Dataoutputverification).size() != 0) {
			String outputtext = driver1.findElement(Dataoutputverification).getText();
			System.out.println("String:"+str);
			//System.out.println(outputtext);
			String msg = str.replaceAll("[()]", "");
			String Input = msg.replaceAll("print", "");
			// System.out.println(output);
			System.out.println(Input + " and " + outputtext + " are same");
		} else {
			System.out.println("Input is not given");
		}
		driver1.manage().timeouts().pageLoadTimeout(Duration.ofSeconds(100));
	}

	public void linked()
	{
		driver1.findElement(Dropdown).click();
		driver1.findElement(Linked).click();

	}



}
