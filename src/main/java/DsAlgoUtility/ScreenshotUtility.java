package DsAlgoUtility;

import java.io.File;
import java.io.IOException;
import java.util.Date;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;


import DsAlgopages.DSAlgoBrowser;


public class ScreenshotUtility extends DSAlgoBrowser{
	
	
 public void getScreenshot() throws IOException 
  {
	  Date Currentdate=new Date();
	  String Screenshotfilenane=Currentdate.toString().replace(":","_"); 
	  File ScreenshotFile=((TakesScreenshot)driver1).getScreenshotAs(OutputType.FILE);
	  FileUtils.copyFile(ScreenshotFile, new File("./Screenshot/"+Screenshotfilenane+".png"));
	
  }
	 
}
