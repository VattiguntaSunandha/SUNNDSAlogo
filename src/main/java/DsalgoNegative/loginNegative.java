package DsalgoNegative;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import Assignments.Browser;

public class loginNegative extends Browser{

	public static WebElement signin;
	public static WebElement Username1;
	public static WebElement Password1;
	public static WebElement login1;

	@Test(priority = 2)
	public static void UserNegative()
	{
		signin = driver1.findElement(By.xpath("//*[contains(text( ),'Sign in')]"));
		signin.click();
		Username1 = driver1.findElement(By.name("username"));
		Username1.sendKeys("svattig@gmail.com");
		Password1 = driver1.findElement(By.name("password"));
		Password1.sendKeys("DSALGO1@");
		login1 = driver1.findElement(By.xpath("//input[@type= 'submit' and @value='Login']"));
		login1.click();
		System.out.println("Invalid User");
	}

	@Test(priority = 3)
	public static void passwordNegative()
	{
		signin = driver1.findElement(By.xpath("//*[contains(text( ),'Sign in')]"));
		signin.click();
		Username1 = driver1.findElement(By.name("username"));
		Username1.sendKeys("svatt@gmail.com");
		Password1 = driver1.findElement(By.name("password"));
		Password1.sendKeys("DSALGO1!");
		login1 = driver1.findElement(By.xpath("//input[@type= 'submit' and @value='Login']"));
		login1.click();
		System.out.println("Invalid Password");
	}

	@Test(priority = 4)
	public static void user_passNegative()
	{
		signin = driver1.findElement(By.xpath("//*[contains(text( ),'Sign in')]"));
		signin.click();
		Username1 = driver1.findElement(By.name("username"));
		Username1.sendKeys("svattig@gmail.com");
		Password1 = driver1.findElement(By.name("password"));
		Password1.sendKeys("DSALGO1!");
		login1 = driver1.findElement(By.xpath("//input[@type= 'submit' and @value='Login']"));
		login1.click();
		System.out.println("Invalid User and Password");
	}
}

